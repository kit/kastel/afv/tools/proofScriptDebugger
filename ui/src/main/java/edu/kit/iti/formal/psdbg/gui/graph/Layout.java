package edu.kit.iti.formal.psdbg.gui.graph;

public abstract class Layout {
    public abstract void execute();
}