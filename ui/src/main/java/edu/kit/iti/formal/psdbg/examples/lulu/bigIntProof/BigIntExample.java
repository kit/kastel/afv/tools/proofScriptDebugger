package edu.kit.iti.formal.psdbg.examples.lulu.bigIntProof;

import edu.kit.iti.formal.psdbg.examples.Example;

public class BigIntExample extends Example {
    public BigIntExample() {
        setName("BigInt");
        defaultInit(getClass());
    }

}
