package edu.kit.iti.formal.psdbg;

import edu.kit.iti.formal.psdbg.interpreter.dbg.DebuggerCommand;
import edu.kit.iti.formal.psdbg.interpreter.dbg.DebuggerFramework;

public class HardStopCommand<T> extends DebuggerCommand<T> {
    @Override
    public void execute(DebuggerFramework<T> dbg) {

    }
}
